////////////////////////////////////////////////////////////
// CANVAS LOADER
////////////////////////////////////////////////////////////

 /*!
 * 
 * START CANVAS PRELOADER - This is the function that runs to preload canvas asserts
 * 
 */
function initPreload(){
	toggleLoader(true);
	
	checkMobileEvent();
	
	$(window).resize(function(){
		resizeGameFunc();
	});
	resizeGameFunc();
	
	loader = new createjs.LoadQueue(false,null);
	manifest=[
			//{src:assetspath+'background.png', id:'background',crossOrigin:true},
			{src:assetspath+'logo.png', id:'logo',crossOrigin:true},
			{src:assetspath+'button_start.png', id:'buttonStart',crossOrigin:true},
			
			{src:assetspath+'bg_wheel.png', id:'bgWheel',crossOrigin:true},
			{src:assetspath+'item_wheel_center.png', id:'itemWheelCentre',crossOrigin:true},
			{src:assetspath+'item_arrow.png', id:'itemArrow',crossOrigin:true},
			{src:assetspath+'item_wheel.png', id:'itemWheel',crossOrigin:true},
			{src:assetspath+'item_pin.png', id:'itemPin',crossOrigin:true},
			{src:assetspath+'item_light.png', id:'itemLight',crossOrigin:true},
			
			{src:assetspath+'item_side.png', id:'itemSide',crossOrigin:true},
			{src:assetspath+'item_game1.png', id:'itemGame1',crossOrigin:true},
			{src:assetspath+'item_game2.png', id:'itemGame2',crossOrigin:true},
			{src:assetspath+'item_ticket.png', id:'itemTicket',crossOrigin:true},
			{src:assetspath+'button_spin.png', id:'buttonSpin',crossOrigin:true},
			
			{src:assetspath+'button_plus.png', id:'buttonPlus',crossOrigin:true},
			{src:assetspath+'button_minus.png', id:'buttonMinus',crossOrigin:true},
			
			{src:assetspath+'button_replay.png', id:'buttonReplay',crossOrigin:true},
			{src:assetspath+'button_facebook.png', id:'buttonFacebook',crossOrigin:true},
			{src:assetspath+'button_twitter.png', id:'buttonTwitter',crossOrigin:true},
			{src:assetspath+'button_google.png', id:'buttonGoogle',crossOrigin:true},
			{src:assetspath+'button_fullscreen.png', id:'buttonFullscreen',crossOrigin:true},
			{src:assetspath+'button_sound_on.png', id:'buttonSoundOn',crossOrigin:true},
			{src:assetspath+'button_sound_off.png', id:'buttonSoundOff',crossOrigin:true},
			{src:assetspath+'cannon.png', id:'cannon',crossOrigin:true}];
			
	for(var n=0;n<wheel_arr.length;n++){
		manifest.push({src:wheel_arr[n].src, id:'wheel'+n});
		manifest.push({src:wheel_arr[n].highlight, id:'wheelH'+n});
	}
	
	for(var n=0;n<wheelSecond_arr.length;n++){
		manifest.push({src:wheelSecond_arr[n].src, id:'wheelInner'+n});
		manifest.push({src:wheelSecond_arr[n].highlight, id:'wheelInnerH'+n});
	}
	
	soundOn = true;
	if($.browser.mobile || isTablet){
		if(!enableMobileSound){
			soundOn=false;
		}
	}
	
	if(soundOn){
		manifest.push({src:assetspath+'sounds/click.ogg', id:'soundClick',crossOrigin:true});
		manifest.push({src:assetspath+'sounds/loss.ogg', id:'soundLoss',crossOrigin:true});
		manifest.push({src:assetspath+'sounds/win.ogg', id:'soundWin',crossOrigin:true});
		manifest.push({src:assetspath+'sounds/lossall.ogg', id:'soundLossall',crossOrigin:true});
		manifest.push({src:assetspath+'sounds/jackpot.ogg', id:'soundJackpot',crossOrigin:true});
		manifest.push({src:assetspath+'sounds/spin.ogg', id:'soundSpin',crossOrigin:true});
		manifest.push({src:assetspath+'sounds/spinning.ogg', id:'soundSpinning',crossOrigin:true});
		manifest.push({src:assetspath+'sounds/ticket.ogg', id:'soundTicket',crossOrigin:true});
		manifest.push({src:assetspath+'sounds/tone.ogg', id:'soundTone',crossOrigin:true});
		manifest.push({src:assetspath+'sounds/result.ogg', id:'soundResult',crossOrigin:true});
		manifest.push({src:assetspath+'sounds/arrow.ogg', id:'soundArrow',crossOrigin:true});
		manifest.push({src:assetspath+'sounds/select.ogg', id:'soundSelect',crossOrigin:true});
		manifest.push({src:assetspath+'sounds/start.ogg', id:'soundStart',crossOrigin:true});
		
		createjs.Sound.alternateExtensions = ["mp3"];
		loader.installPlugin(createjs.Sound);
	}
	
	loader.addEventListener("complete", handleComplete);
	loader.addEventListener("fileload", fileComplete);
	loader.addEventListener("error",handleFileError);
	loader.on("progress", handleProgress, this);
	loader.loadManifest(manifest);
}

/*!
 * 
 * CANVAS FILE COMPLETE EVENT - This is the function that runs to update when file loaded complete
 * 
 */
function fileComplete(evt) {
	var item = evt.item;
	//console.log("Event Callback file loaded ", evt.item.id);
}

/*!
 * 
 * CANVAS FILE HANDLE EVENT - This is the function that runs to handle file error
 * 
 */
function handleFileError(evt) {
	console.log("error ", evt);
}

/*!
 * 
 * CANVAS PRELOADER UPDATE - This is the function that runs to update preloder progress
 * 
 */
function handleProgress() {
	$('#mainLoader span').html(Math.round(loader.progress/1*100)+'%');
}

/*!
 * 
 * CANVAS PRELOADER COMPLETE - This is the function that runs when preloader is complete
 * 
 */
function handleComplete() {
	toggleLoader(false);	
	if(typeof(player)!=='undefined'){
		initMain();
	}else{
		alert("Este jugador no se encuentra en la base de datos");
	}	
};

/*!
 * 
 * TOGGLE LOADER - This is the function that runs to display/hide loader
 * 
 */
function toggleLoader(con){
	if(con){
		$('#mainLoader').show();
	}else{
		$('#mainLoader').hide();
	}
}