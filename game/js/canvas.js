////////////////////////////////////////////////////////////
// CANVAS
////////////////////////////////////////////////////////////
var stage
var canvasW=0;
var canvasH=0;

/*!
 * 
 * START GAME CANVAS - This is the function that runs to setup game canvas
 * 
 */
function initGameCanvas(w,h){
	var gameCanvas = document.getElementById("gameCanvas");
	gameCanvas.width = w;
	gameCanvas.height = h;
	
	canvasW=w;
	canvasH=h;
	stage = new createjs.Stage("gameCanvas");
	
	createjs.Touch.enable(stage);
	stage.enableMouseOver(20);
	stage.mouseMoveOutside = true;
	
	createjs.Ticker.setFPS(60);
	createjs.Ticker.addEventListener("tick", tick);	
}

var guide = false;
var resultCreditBg,itemCreditBg,canvasContainer,confettiContainer,cannonContainer, mainContainer, gameContainer, wheelOuterContainer, wheelInnerContainer, lightsContainer,ticketContainer, resultContainer, munecoContainer;
var cannon, guideline, bg, logo, buttonStart, buttonReplay, buttonFacebook, buttonTwitter, buttonGoogle, buttonFullscreen, buttonSoundOn, buttonSoundOff;

$.wheel = {};
$.wheelInner = {};
$.ticket = {};
$.light = {};

/*!
 * 
 * BUILD GAME CANVAS ASSERTS - This is the function that runs to build game canvas asserts
 * 
 */
function buildGameCanvas(){
	canvasContainer = new createjs.Container();
	mainContainer = new createjs.Container();
	gameContainer = new createjs.Container();
	wheelContainer = new createjs.Container();
	wheelOuterContainer = new createjs.Container();
	wheelInnerContainer = new createjs.Container();
	wheelPinContainer = new createjs.Container();
	lightsContainer = new createjs.Container();
	ticketContainer = new createjs.Container();
	resultContainer = new createjs.Container();	

	itemWheel = new createjs.Bitmap(loader.getResult('itemWheel'));
	centerReg(itemWheel);
	itemWheelCentre = new createjs.Bitmap(loader.getResult('itemWheelCentre'));
	centerReg(itemWheelCentre);
	itemWheel.x = itemWheelCentre.x = wheelOuterContainer.x = wheelInnerContainer.x = wheelPinContainer.x = lightsContainer.x = wheelX;
	itemWheel.y = itemWheelCentre.y = wheelOuterContainer.y = wheelInnerContainer.y = wheelPinContainer.y = lightsContainer.y = wheelY;
	
	itemArrow = new createjs.Bitmap(loader.getResult('itemArrow'));
	itemArrow.regX = 27;
	itemArrow.regY = 14;
	itemArrow.x = arrowX;
	itemArrow.y = arrowY;
	
	itemPin = new createjs.Bitmap(loader.getResult('itemPin'));
	centerReg(itemPin);
	itemPin.x = -500;
	
	var _frameW=22;
	var _frameH=22;
	var _frame = {"regX": _frameW/2, "regY": _frameH/2, "height": _frameH, "count": 2, "width": _frameW};
	var _animations = {off:{frames: [0], speed:1},
						on:{frames: [1], speed:1}};
						
	itemLightData = new createjs.SpriteSheet({
		"images": [loader.getResult("itemLight").src],
		"frames": _frame,
		"animations": _animations
	});

	itemLightAnimate = new createjs.Sprite(itemLightData, "off");
	itemLightAnimate.framerate = 20;
	itemLightAnimate.x = -100;
	
	if(guide){
		guideline = new createjs.Shape();	
		guideline.graphics.setStrokeStyle(2).beginStroke('red').drawRect((stageW-contentW)/2, (stageH-contentH)/2, contentW, contentH);
	}

	wheelContainer.addChild(wheelOuterContainer, wheelInnerContainer, itemWheel, lightsContainer, itemArrow, wheelPinContainer);
	gameContainer.addChild(itemPin,itemLightAnimate);
	
	canvasContainer.addChild(wheelContainer, mainContainer, gameContainer, resultContainer, guideline);
	stage.addChild(canvasContainer);
	
	resizeCanvas();
}


/*!
 * 
 * RESIZE GAME CANVAS - This is the function that runs to resize game canvas
 * 
 */
function resizeCanvas(){
 	if(canvasContainer!=undefined){
		/*buttonSoundOn.x = buttonSoundOff.x = canvasW - offset.x;
		buttonSoundOn.y = buttonSoundOff.y = offset.y;
		buttonSoundOn.x = buttonSoundOff.x -= 40;
		buttonSoundOn.y = buttonSoundOff.y += 30;*/
		
		/*buttonFullscreen.x = buttonSoundOn.x - 63;
		buttonFullscreen.y = buttonSoundOn.y;*/
	}
}

/*!
 * 
 * REMOVE GAME CANVAS - This is the function that runs to remove game canvas
 * 
 */
 function removeGameCanvas(){
	 stage.autoClear = true;
	 stage.removeAllChildren();
	 stage.update();
	 createjs.Ticker.removeEventListener("tick", tick);
	 createjs.Ticker.removeEventListener("tick", stage);
 }

/*!
 * 
 * CANVAS LOOP - This is the function that runs for canvas loop
 * 
 */ 
function tick(event) {
	updateGame();
	stage.update(event);
}

/*!
 * 
 * CANVAS MISC FUNCTIONS
 * 
 */
function centerReg(obj){
	obj.regX=obj.image.naturalWidth/2;
	obj.regY=obj.image.naturalHeight/2;
}

function createHitarea(obj){
	obj.hitArea = new createjs.Shape(new createjs.Graphics().beginFill("#000").drawRect(0, 0, obj.image.naturalWidth, obj.image.naturalHeight));	
}