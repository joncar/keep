DROP VIEW IF EXISTS view_jugadores;
CREATE VIEW view_jugadores AS 
SELECT 
stickers.id,
stickers.codigo,
stickers.numero,
stickers.mensaje,
jugadores.nombre AS jugador_nombre,
jugadores.email AS jugador_email,
jugadores.ciudad AS jugador_ciudad,
jugadores.tipo_persona AS jugador_tipo,
jugadores.celular AS jugador_celular
FROM `stickers`
LEFT JOIN jugadores ON jugadores.codigo = stickers.codigo AND jugadores.resultado = stickers.numero