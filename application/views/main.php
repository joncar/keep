<section>
    <img src="<?= base_url() ?>game/assets/nube.png" alt="" id="nubes">
    <img src="<?= base_url() ?>game/assets/botes.png" alt="" id="botes">
    <div class="content">
        <!-- PERCENT LOADER START-->
        <div id="mainLoader">
            <img src="<?= base_url() ?>game/assets/loader.png" />
            <br><span>0</span>
        </div>
        <div id="mainHolder">
            <!-- BROWSER NOT SUPPORT START-->
            <div id="notSupportHolder">
                <div class="notSupport">
                    LASTIMA. <br/> PARECE QUE TU NAVEGADOR ESTA OBSOLETO, ACTUALIZALO PARA PODER PARTICIPAR                    
                </div>
            </div>
            <div id="rotateHolder">
                <div class="mobileRotate">
                    <div class="rotateDesc">
                        <div class="rotateImg">
                            <img src="<?= base_url() ?>game/assets/rotate.png" />
                        </div>                        
                        Gira el dispositivo
                    </div>
                </div>
            </div>
            <div class="registro">                
                <form onsubmit="registrar(this);return false">                  
                  <div class="form-group">                    
                    <input type="text" class="form-control" name="nombre" id="nombre" placeholder="Nombre completo">
                  </div>   
                  <div class="form-group">                    
                    <input type="email" class="form-control" name="email" id="email" placeholder="Correo electrónico">
                  </div> 
                  <div class="form-group">                    
                    <input type="text" class="form-control" name="ciudad" id="ciudad" placeholder="Ciudad de residencia">
                  </div>
                  <div class="form-group">                    
                    <input type="text" class="form-control" name="tipo_persona" id="tipo_persona" placeholder="Tipo de persona">
                  </div>                                         
                  <div class="form-group">                    
                    <input type="text" class="form-control" name="celular" id="celular" placeholder="Numero de celular">
                  </div>                           
                  <div class="form-group codigoinp">
                    <label for="exampleInputEmail1">Ingresa el código</label>
                    <input type="text" class="form-control" name="codigo" id="codigo" placeholder="Código" size="4" maxlength="4">
                  </div>
                  <div class="registroMsj"></div>
                  <div style="text-align: center">
                    <button type="submit" class="btn btn-default">Registrar</button>
                  </div>
                </form>
            </div>

            <div class="registro" id="resultado">
                  <div>
                        <img src="<?= base_url() ?>game/assets/resultado.png">
                  </div>
            </div>
            <!-- CANVAS START-->
            <div id="canvasHolder">
                <canvas id="gameCanvas" width="768" height="768" crossorigin="Anonymous"></canvas>
            </div>
        </div>                
    </div>
</section>
<script>
    var codigo_juego = 'asdasdasd';
    var player = undefined;
    var URL = 'https://localhost/ruleta/notificaciones/frontend/';
    var lang = 'es';
    var assetspath="<?= base_url() ?>game/assets/";
</script>
<script src="<?= base_url() ?>game/js/vendor/detectmobilebrowser.js"></script>
<script src="<?= base_url() ?>game/js/vendor/createjs-2015.11.26.min.js"></script>
<script src="<?= base_url() ?>game/js/vendor/p2.min.js"></script>
<script src="<?= base_url() ?>game/js/vendor/TweenMax.min.js"></script>
<script src="<?= base_url() ?>game/js/plugins.js"></script>
<script src="<?= base_url() ?>game/js/sound.js"></script>
<script src="<?= base_url() ?>game/js/canvas.js"></script>
<script src="<?= base_url() ?>game/js/p2.js"></script>
<script src="<?= base_url() ?>game/js/confeti.js"></script>        
<script src="<?= base_url() ?>game/js/game.js?v=1"></script>
<script src="<?= base_url() ?>game/js/mobile.js"></script>
<script src="<?= base_url() ?>game/js/main.js"></script>
<script src="<?= base_url() ?>game/js/loader.js"></script>
<script src="<?= base_url() ?>game/js/init.js"></script>
<script src="<?= base_url() ?>js/frame.js"></script>
<script>    
    function updatePlayer(data){
        return true;
    }

    function save(){
        setTimeout(function(){
            $("#canvasHolder").fadeOut(500).html('');
            $("#resultado").fadeIn(500);
            $.post('<?= base_url() ?>game/front/sendMail/'+player,{},function(){});
        },1000);        
    }

    function registrar(f){
        info('.registroMsj','Consultando información por favor espere...');
        f = new FormData(f);
        remoteConnection('game/front/registrar',f,function(data){
            data = JSON.parse(data);
            if(data.success){
                $(".registro").hide();                
                gameChance = 1;
                codigo_juego = data.resultado;
                player = data.jugador;
                $("body").addClass('inGame');
                playGame();
            }else{
                error('.registroMsj',data.msj)
            }
        });        
    }
</script>   