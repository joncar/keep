<!doctype html>
<html lang="en">

	<!-- Google Web Fonts
	================================================== -->

	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i%7CFrank+Ruhl+Libre:300,400,500,700,900" rel="stylesheet">

	<!-- Basic Page Needs
	================================================== -->

	<title><?= empty($title) ? 'Monalco' : $title ?></title>
  	<meta name="keywords" content="<?= empty($keywords) ?'': $keywords ?>" />
	<meta name="description" content="<?= empty($keywords) ?'': $description ?>" /> 	
	<link rel="icon" href="<?= empty($favicon) ?'': base_url().'img/'.$favicon ?>" type="image/x-icon"/>	
	<link rel="shortcut icon" href="<?= empty($favicon) ?'': base_url().'img/'.$favicon ?>" type="image/x-icon"/>
	<link href="<?= base_url() ?>js/stocookie/stoCookie.css" rel="stylesheet">

	<script>var URL = URI = '<?= base_url() ?>';</script>

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <script src="<?= base_url() ?>game/js/vendor/jquery-1.12.4.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>                
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

    <script>
        if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
            var msViewportStyle = document.createElement("style");
            msViewportStyle.appendChild(
                document.createTextNode(
                    "@-ms-viewport{width:device-width}"
                )
            );
            document.getElementsByTagName("head")[0].
                appendChild(msViewportStyle);
        }

    </script>
    <link rel="stylesheet" href="<?= base_url() ?>game/css/normalize.css">
    <link rel="stylesheet" href="<?= base_url() ?>game/css/main.css">
    <script src="<?= base_url() ?>game/js/vendor/modernizr-2.6.2.min.js"></script>
</head>

<body>	
	<?php 
		$this->load->view($view); 		
	?>		    
</body>
</html>