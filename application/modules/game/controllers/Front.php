<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Front extends Main{
        function __construct() {
            parent::__construct();
            $this->load->library('form_validation');
        }

        function registrar(){            
            //Validar formulario
            $response = array('success'=>false,'msj'=>'Debe indicar los datos solicitados');
            $datos = $this->db->field_data('jugadores');            
            foreach($datos as $d){
            	if($d->name!='id' && $d->null=='NO'){                 	       	
            		$this->form_validation->set_rules($d->name,$d->name,'required');
            	}
            }

            if($this->form_validation->run()){
            	$codigo = $this->db->get_where('stickers',array('codigo'=>$_POST['codigo']));
            	$jugador = $this->db->get_where('jugadores',array('email'=>$_POST['email'],'codigo'=>$_POST['codigo']));
            	$this->db->where("jugador_email IS NULL AND codigo = '".$_POST['codigo']."'",NULL,TRUE);
            	$disponible = $this->db->get_where('view_jugadores');
            	if($codigo->num_rows()==0){
            		$response['msj'] = 'No encontramos el código ingresado';	
            	}
            	else{
            		if($jugador->num_rows()==0){
            			//No ha jugado
            			if($disponible->num_rows()>0){
	            			$this->db->insert('jugadores',array(
	            				'nombre'=>$_POST['nombre'],
	            				'email'=>$_POST['email'],
	            				'ciudad'=>$_POST['ciudad'],
	            				'tipo_persona'=>$_POST['tipo_persona'],
	            				'celular'=>$_POST['celular'],
	            				'codigo'=>$disponible->row()->codigo,
	            				'resultado'=>$disponible->row()->numero,
	            				'mensaje'=>$disponible->row()->mensaje
	            			));
	            			$response['success'] = true;
	            			$response['resultado'] = $disponible->row()->numero-1;
	            			$response['jugador'] = $this->db->insert_id();
            			}else{
            				$response['msj'] = 'No encontramos el código ingresado';	
            			}
            		}else{
            			//Ya jugo
            			$response['msj'] = 'Ya has participado, te hemos reenviado el correo electrónico por si aún no te ha llegado';
            			$this->sendMail($jugador->row()->id);
            		}
            	}
            	//$response = array('success'=>true);
            }else{
            	$response['msj'] = $this->form_validation->error_string();
            }

            echo json_encode($response);
        }

        function sendMail($jugador){
        	$jugador = $this->db->get_where('jugadores',array('id'=>$jugador));
        	if($jugador->num_rows()>0){
        		$jugador->row()->imagen = '<img src="'.base_url().'game/mailing/'.$jugador->row()->resultado.'.jpg">';
        		$this->enviarcorreo($jugador->row(),1);
        	}
        }
    }
?>
