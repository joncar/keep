<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Game extends Panel{
        function __construct() {
            parent::__construct();
        }

        function stickers(){   
            $this->as['stickers'] = 'view_jugadores';
            $crud = $this->crud_function('','');   
            $crud->set_primary_key('id');          
            $this->loadView($crud->render());
        }
        
        function jugadores(){
            $crud = $this->crud_function('','');             
            $this->loadView($crud->render());
        }
    }
?>
